@extends('layout.index')

@section('judul')
Halaman Cast 
@endsection

@section('content')

<a href= "/cast/create"  class="btn btn-primary btn-sm my-3">Tambah</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
 
    
  <tbody>
  @forelse ($cast as $key => $items)

    <tr>
     <td>{{$key + 1}}</td>
     <td>{{$items ->nama}}</td>
     <td>
        
        <form action="/cast/{{$items->id}}" method="POST">
        <a href="/cast/{{$items->id}}" class= "btn btn-info btn-sm">Detail</a>
        <a href="/cast/{{$items->id}}/edit" class= "btn btn-success btn-sm">Edit</a>
            @method('delete')
            @csrf
            <input type="submit" value="delete" class="btn btn-danger btn-sm">
        </form>

     </td>
    </tr>
    @empty
    @endforelse
  </tbody>
</table>
@endsection