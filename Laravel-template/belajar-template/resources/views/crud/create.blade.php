@extends('layout.index')

@section('judul')
Halaman Data Table 
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="">Nama</label>
        <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
     @enderror
    <div class="form-group">
        <label for="">Umur</label>
        <input type="integer" name="umur" class="form-control">
    </div>
    <div class="form-group">
        <label for="">Bio</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
    </div><br><br>
    @error('deskripsi')
        <div class="alert alert-danger">
         {{ $message }}
        </div>
    @enderror
    <button type="submit" class="btn btn-success">submit</button>
</form>
@endsection