<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('partial.data');
    }

    public function utama(){
      return view('partial.data-table');
    }

    public function profile(){
      return view('profile');
  }
    
}
