<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create(){
        return view ('crud.create');
    }

    public function store(Request $request){
        
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama','required' => 'nama harus diisi',
            'bio','required' => 'bip harus diisi'
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');

    }

    public function index(){
      $cast = DB::table('cast')->get();
    //   dd($cast);
      return view('crud.read', ['cast'=> $cast]);
    }

    public function show($id){
        $cast = DB::table('cast')->find($id);
        return view ('crud.detail',['cast'=>$cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->find($id);
        return view ('crud.edit',['cast'=>$cast]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
        ],
        [
            'nama','required' => 'nama harus diisi',
            'bio','required' => 'bip harus diisi'
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]);

            return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id',$id)->delete();

        return redirect('/cast');
    }
}
