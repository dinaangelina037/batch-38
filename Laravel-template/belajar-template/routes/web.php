<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/data',[HomeController::class, 'index'])->name('data');
Route::get('/data-table',[HomeController::class, 'utama'])->name('data-table');
Route::get('/profile',[HomeController::class, 'profile'])->name('profile');


// CRUD 
Route::get('/cast/create',[CastController::class, 'create']);
Route::post('/cast',[CastController::class, 'store']);

// read
Route::get('/cast',[CastController::class, 'index']);
Route::get('/cast/{cast_id}',[CastController::class, 'show']);
// update
Route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']);
Route::put('/cast/{cast_id}',[CastController::class, 'update']);
// delete
Route::delete('/cast/{cast_id}',[CastController::class, 'destroy']);


