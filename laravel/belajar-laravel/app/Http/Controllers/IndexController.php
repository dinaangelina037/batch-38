<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function welcome(Request $request){
        $firstname = $request['depan'];
        $lastname = $request['belakang'];
    
        return view('welcome',['firstname' => $firstname, 'lastname' => $lastname]);
        
      }
}
